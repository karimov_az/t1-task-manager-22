package ru.t1.karimov.tm.command.user;

import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOCK");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeOneByLogin(login);
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "User remove.";
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
