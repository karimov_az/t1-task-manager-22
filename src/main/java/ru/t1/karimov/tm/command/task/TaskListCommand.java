package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[TASKS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final List<Task> tasks;
        if (sort == null) tasks = getTaskService().findAll(userId);
        else tasks = getTaskService().findAll(userId, sort.getComparator());
        renderTasks(tasks);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

}
