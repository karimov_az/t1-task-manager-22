package ru.t1.karimov.tm.command.user;

import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOCK");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public String getDescription() {
        return "User lock.";
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
