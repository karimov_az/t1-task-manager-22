package ru.t1.karimov.tm.command;

import ru.t1.karimov.tm.api.model.ICommand;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute() throws AbstractException;

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() throws AbstractException {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final String colonComma = (argument == null || argument.isEmpty()) ? " : " : " , ";
        if (name != null && !name.isEmpty()) result += name + colonComma;
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
