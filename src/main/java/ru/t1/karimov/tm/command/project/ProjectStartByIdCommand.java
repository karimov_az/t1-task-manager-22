package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }

}
