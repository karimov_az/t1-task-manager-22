package ru.t1.karimov.tm;

import ru.t1.karimov.tm.component.Bootstrap;
import ru.t1.karimov.tm.exception.AbstractException;

public final class Application {

    public static void main(final String[] args) throws AbstractException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
