package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task, ITaskRepository> {

    Task create(String userId, String name, String description) throws AbstractFieldException;

    Task create(String userId, String name) throws AbstractFieldException;

    List<Task> findAllByProjectId(String userId, String projectId) throws AbstractFieldException;

    Task updateById(String userId, String id, String name, String description) throws AbstractException;

    Task updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task changeTaskStatusById (String userId, String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex (String userId, Integer index, Status status) throws AbstractException;

}
