package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    User removeOneByLogin(String login) throws AbstractException;

    User removeOneByEmail(String email) throws AbstractException;

    User setPassword(String id, String password) throws AbstractException;

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    ) throws AbstractException;

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    void lockUserByLogin(String login) throws AbstractException;

    void unlockUserByLogin(String login) throws AbstractException;

}
