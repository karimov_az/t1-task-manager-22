package ru.t1.karimov.tm.api.model;

import ru.t1.karimov.tm.exception.AbstractException;

public interface ICommand {

    void execute() throws AbstractException;

    String getName();

    String getArgument();

    String getDescription();

}
