package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.IUserOwnedRepository;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends IService<M> {

    List<M> findAll(String userId) throws AbstractFieldException;

    List<M> findAll(String userId, Comparator<M> comparator) throws AbstractFieldException;

    boolean existsById(String userId, String id) throws AbstractFieldException;

    M findOneById(String userId, String id) throws AbstractFieldException;

    M findOneByIndex(String userId, Integer index) throws AbstractFieldException;

    int getSize(String userId) throws AbstractFieldException;

    void removeAll(String userId) throws AbstractFieldException;

    M removeOneById(String userId, String id) throws AbstractFieldException;

    M removeOneByIndex(String userId, Integer index) throws AbstractFieldException;

    M add(String userId, M model) throws AbstractFieldException;

    M removeOne(String userId, M model) throws AbstractFieldException;

}
