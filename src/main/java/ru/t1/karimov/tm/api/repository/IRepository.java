package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model) throws AbstractException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    M removeOne(M model) throws AbstractEntityNotFoundException;

    M removeOneById(String id) throws AbstractFieldException;

    M removeOneByIndex(Integer index) throws AbstractFieldException;

    void removeAll();

    int getSize();

    boolean existsById(String id);

}
